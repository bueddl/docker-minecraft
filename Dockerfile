FROM openjdk:12-oracle
MAINTAINER "Sebastian Büttner <sebastian@bueddl.de>"

EXPOSE 25565

RUN yum install -y wget screen unzip
COPY data/entrypoint.sh /
COPY data/entrypoint.sh /

RUN useradd -U -u 1000 -s /bin/false -m minecraft

WORKDIR /home/minecraft
USER minecraft

RUN wget https://launcher.mojang.com/v1/objects/ed76d597a44c5266be2a7fcd77a8270f1f0bc118/server.jar

RUN mkdir world logs

ENTRYPOINT ["/entrypoint.sh", "screen", "-S", "minecraft", "java", "-jar", "server.jar", "nogui"]
CMD ["-Xmx1024M", "-Xms1024M"]
